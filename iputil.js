var my_iputil = {};
my_iputil.expandIPv6 = function(ostr) {
	let expandZero = function(i, n) {
		let s = String(i);
		while (s.length < n) {
			s = "0" + s;
		}
		return s;
	}
	let str = String(ostr);
	let m = str.match(/^(.*):([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)$/);
	if (m) {
		let a = [Number(m[2]), Number(m[3]), Number(m[4]), Number(m[5])];
		if (!(a[0] < 256)) return null;
		if (!(a[1] < 256)) return null;
		if (!(a[2] < 256)) return null;
		if (!(a[3] < 256)) return null;
		str = m[1] + ":" + (a[0] * 256 + a[1]).toString(16) + ":" + (a[2] * 256 + a[3]).toString(16);
	}
	if (str === "::") return ["0000", "0000", "0000", "0000", "0000", "0000", "0000", "0000"];
	m = str.match(/^[0-9a-fA-F:]*$/);
	if (!m) return null;
	let dc1 = str.indexOf("::");
	let dc2 = str.lastIndexOf("::");
	if (dc1 !== dc2) return null;
	if (dc1 >= 0) {
		/* count the number of :'s, there should be exactly 7. */
		let nr_colon = -2;
		for (let i of str) {
			if (i === ':') nr_colon++;
		}
		if (nr_colon < 0 || nr_colon > 6) return null;
		let fix_l = ["0:", "0:0:", "0:0:0:", "0:0:0:0:", "0:0:0:0:0:", "0:0:0:0:0:0:", "0:0:0:0:0:0:0:"];
		let fix_c = [":", ":0:", ":0:0:", ":0:0:0:", ":0:0:0:0:", ":0:0:0:0:0:", ":0:0:0:0:0:0:"];
		let fix_r = [":0", ":0:0", ":0:0:0", ":0:0:0:0", ":0:0:0:0:0", ":0:0:0:0:0:0", ":0:0:0:0:0:0:0"];
		let which_p = (dc1 === 0 ? fix_l : dc1 === (str.length - 2) ? fix_r : fix_c)[6 - nr_colon];
		str = str.replace("::", which_p);
	}
	str = str.toLowerCase();
	let p = /^([0-9a-f]{1,4}):([0-9a-f]{1,4}):([0-9a-f]{1,4}):([0-9a-f]{1,4}):([0-9a-f]{1,4}):([0-9a-f]{1,4}):([0-9a-f]{1,4}):([0-9a-f]{1,4})$/;
	let x = str.match(p);
	if (x) {
		let r = [];
		for (let i = 1; i <= 8; i++) {
			r.push(expandZero(x[i], 4));
		}
		return r;
	}
	return null;
}
// exports.iputil = my_iputil;
